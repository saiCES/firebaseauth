import Vue from "vue";
import './plugins/fontawesome'
import App from "./App.vue";
import router from "./router";
import store from "./store/index";
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'vue2-animate/dist/vue2-animate.css'
import Vuelidate from 'vuelidate'
import vuetify from './plugins/vuetify';
import {authRef} from './api/firebaseconfig'

Vue.use(Vuelidate)
Vue.use(require('vue-moment'));


Vue.config.productionTip = false;

let app = '';

authRef.onAuthStateChanged((user) => {
  if (user) {
    store.dispatch("auth/getCurrentUser");
    store.commit("auth/setfirebaseLoggedinUser",user);
    setTimeout(
      () => {
        if(store.state.auth.currentUser.status == 1) {
          store.dispatch('forum/getUserForums', store.state.auth.currentUser);
        }
      }, 
      1000
    )
  } else {
   
  }
  if (!app) {
app = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
}});

