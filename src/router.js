import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
// import Home from "./views/Home.vue";
import AddForum from "./components/forum/myForum/AddForum";
import UserForum from "./components/forum/myForum/UserForum";
import EditForum from "./components/forum/myForum/EditForum";
import firebase from 'firebase';
import ListForumPosts from "./components/forum/ListForumPosts";
import AddPost from "./components/posts/AddPost";
import UserPosts from "./components/posts/UserPosts";
import ViewPost from "./components/posts/ViewPost";
import EditPost from "./components/posts/EditPost";
import ListForum from "./components/forum/ListForum"

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/login",
      name: "login",
      component: Login,
    

    },
    {
      path: "/register",
      name: "register",
      component: Register,
     
    },
    {
      path: "/",
      name: 'ListForum',
      component: ListForum,
      meta: {
        requiresAuth: true
      }


    },
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/add-forum',
      name: 'AddForum',
      component: AddForum,
      meta: {
        requiresAuth: true
      }

    },
    {
      path: '/my-forum',
      name: 'UserForum',
      component: UserForum,
      meta: {
        requiresAuth: true
      }

    },
    {
      path: '/user-posts/:id',
      name: 'UserPosts',
      component: UserPosts,
      meta: {
        requiresAuth: true
      }

    },
    {
      path: '/edit-forum/:id',
      name: 'EditForum',
      component: EditForum,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/listforum-posts/:id',
      name:'ListForumPosts',
      component:ListForumPosts,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/add-post/:id',
      name:'AddPost',
      component:AddPost,
      meta: {
        requiresAuth: true
      }
    },  {
      path: '/edit-post/:id',
      name:'EditPost',
      component:EditPost,
      meta: {
        requiresAuth: true
      }
    },
    {
      path:'/view-post/:id',
      name:'ViewPost',
      component:ViewPost,
      meta:{
        requiresAuth:true
      }
    }


  ]
});



router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('login');
  else if (!requiresAuth && currentUser) next('home');
  else next();
});


export default router;