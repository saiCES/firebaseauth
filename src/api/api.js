import { authRef, db } from './firebaseconfig'

export default {

    /*--  Authentication --*/
    checkCurrentUser() {
        return authRef.currentUser
    },
    forgotPassword(emailAddress, successcallback, errorcallback) {
        authRef.sendPasswordResetEmail(emailAddress).then(successcallback).catch(errorcallback);
    },
    signIn(email, password, successcallback, errorcallback) {
        authRef.signInWithEmailAndPassword(email, password).then(successcallback).catch(errorcallback);
    },
    logOut(successcallback, errorcallback) {
        authRef.signOut().then(successcallback).catch(errorcallback);
    },
    getCurrentUser(callback) {
        authRef.onAuthStateChanged(callback);
    },
    signUp(email, password, successcallback, errorcallback) {
        authRef.createUserWithEmailAndPassword(email, password).then(successcallback).catch(errorcallback);
    },
    updateUserDisplayname(name) {
        var user = authRef.currentUser;

        user.updateProfile({
            displayName: name,
        }).then(function () {
        }).catch(function (error) {
        });

    },
    /*-- Adding user into firebase DB  --*/
    addUser(name, email, uid) {
        var usersRef = db.ref('users');
        var usersPush = usersRef.push();

        const key = usersPush.getKey();

        usersPush.set({
            name: name,
            email: email,
            uid: uid,
            created_at: (new Date()).toLocaleString()
        });
        return key;
    },
    fetchUserByUID(UID, callback) {
        var userRef = db.ref('users').orderByChild("uid").equalTo(UID);

        userRef.on('value', function (snapshot) {

            if (snapshot.val() != null) {
                callback(Object.keys(snapshot.val())[0], snapshot.val());

            } else {
                callback(null, null);
            }
        });
    },

    /* -- Adding Data to Forums  ---  */

    addForum(title, content, user_id) {
        var forumRef = db.ref('forums');
        var forumPush = forumRef.push();

        forumPush.set({
            title: title,
            content: content,
            user_id: user_id,
            created_at: (new Date()).toLocaleString()
        });

    },

    updateForum(title, content, key) {
        var updates = {
            title: title,
            content: content
        }
        db.ref('forums/' + key).update(updates);
    },
    deleteForum(key) {
        db.ref('forums/' + key).remove();
    },
    fetchForumbyUID(user_id, callback) {
        var forumByUIDRef = db.ref('forums').orderByChild("user_id").equalTo(user_id);
        forumByUIDRef.on('value', function (snapshot) {

            if (snapshot.val() != null) {
                callback(snapshot.val());

            } else {
                callback(null, null);
            }
        });
    },
    fetchForumByKey(key, callback) {

        var itemRef = db.ref('forums').child(key);

        itemRef.once('value', function (snapshot) {
            callback(snapshot.val(), 'forum')
        });

    },

    fetchPostsByKey(key, callback) {
        var postsRef = db.ref('posts').orderByChild("forum_id").equalTo(key);
        postsRef.on('value', function (snapshot) {

            callback(snapshot.val(), 'posts')
        });
    },


    retrieveRespectivePostByKey(key, callback) {

        var postRef = db.ref('posts').child(key);

        postRef.on('value', function (snapshot) {

            var postVal = snapshot.val()

            var userRef = db.ref('users').child(postVal.user_id)

            userRef.on('value', function (snapshotUser) {

                var forumRef = db.ref('forums').child(postVal.forum_id)

                forumRef.on('value', function (snapshotForum) {

                    callback(postVal, snapshotUser.val(), snapshotForum.val())
                });
            });
        });
    },

    getForums(callback) {
        var ref = db.ref("forums");

        ref.once("value", function (snapshot) {

            if (snapshot.val() != null) {
                callback(snapshot.val(), 'forum')

            }

        })
    },
    userForums(user_id, callback) {
        var forumRef = db.ref('forums').orderByChild("user_id").equalTo(user_id);

        forumRef.on('value', function (snapshot) {
            callback(snapshot.val());
        });
    },

    getMyPosts(forum_key, user_key, callback) {
        var PostsRef = db.ref('posts').orderByChild("userId_forumId").equalTo(user_key + "_" + forum_key);

        PostsRef.on('value', function (snapshot) {
            callback(snapshot.val());
        });
    },

    addPost(title, content, forum_id, user_id, userdetail) {
        var forumRef = db.ref('posts');
        var forumPush = forumRef.push();

        forumPush.set({
            title: title,
            content: content,
            user_id: user_id,
            created_at: (new Date()).toLocaleString(),
            forum_id: forum_id,
            view_count: 0,
            userId_forumId: user_id + "_" + forum_id,
            userdetail: userdetail
        });


    },
    updatePost(title, content, key) {

        db.ref("posts/" + key).update({ title: title, content: content });
    },
    deletePost(key) {
        db.ref("posts/" + key).remove();
    },

    loadAllForums(callback) {
        var ref = db.ref("forums");

        ref.once("value", (snapshot) => {

            if (snapshot.val() != null) {
                snapshot.forEach(function (forum) {
                    var forumKey = forum.key;
                    var forumData = forum.val();
                    forumData.key = forumKey;
                    forumData.posts = [];

                    var postsRef = db.ref("posts").orderByChild("forum_id").equalTo(forumKey);


                    postsRef.on("child_added", function (posts) {
                        var postsData = posts.val();
                        postsData.key = posts.key;
                        postsData.username = "";

                        var usersRef = db.ref("users/" + postsData.user_id);

                        usersRef.on("value", function (user) {
                            postsData.username = user.val().name;

                            forumData.posts.push(postsData);
                        });
                    });
                    callback(forumData);
                });
            } else {
                callback(null);
            }

        }, (error) => {

        });
    },
    updateViews(key) {
        var postRef = db.ref("posts").child(key).child('view_count');
        postRef.transaction(function (views) {
            views = views + 1
            return views
        })
    }

}