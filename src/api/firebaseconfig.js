import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyCxI7engoLJI1yJsMWVNA11zrUgYpu6LHI",
    authDomain: "vue-forum-33303.firebaseapp.com",
    databaseURL: "https://vue-forum-33303.firebaseio.com",
    projectId: "vue-forum-33303",
    storageBucket: "vue-forum-33303.appspot.com",
    messagingSenderId: "70943310792",
    appId: "1:70943310792:web:e7c94e041b6cc3b45a36fd"
};

firebase.initializeApp(firebaseConfig);

export const authRef = firebase.auth();

export const db = firebase.database();
