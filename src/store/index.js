import Vue from "vue";
import Vuex from "vuex";
import auth from './auth'
import forum from './forum'
import posts from './posts'


Vue.use(Vuex);


export default new Vuex.Store({
    modules: {
        auth: { ...auth, namespaced: true},
        forum: { ...forum, namespaced: true},
        posts: {...posts, namespaced: true},
    }
})