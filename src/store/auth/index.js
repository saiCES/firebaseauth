import api from "../../api/api";

export default {
    state: {
        auth: {
            email: "",
            fullname: ""
        },
        currentUser: {
            id: "",
            name: "",
            email: "",
            uid: "",
            status: 0
        },
        firbaseLoggedinUser: "",

    },
    getters: {
        getCurrentUser(state) {
            return state.currentUser;
        },

    },
    mutations: {
        setAuthEmail(state, data) {
            state.auth.email = data
        },
        setAuthName(state, data) {
            state.auth.fullname = data
        },
        setCurrentUser(state, currUserData) {
            state.currentUser.id = currUserData.key,
                state.currentUser.name = currUserData.name,
                state.currentUser.email = currUserData.email,
                state.currentUser.uid = currUserData.uid,
                state.currentUser.status = currUserData.status
        },
        setfirebaseLoggedinUser(state, user) {
            state.firbaseLoggedinUser = user;
        },

    },
    actions: {
        // On refresh get current user and store it in the state
        getCurrentUser({ commit }) {
            api.getCurrentUser((user) => {
                if (user) {
                    api.fetchUserByUID(user.uid, (key, val) => {
                        if (key != null && val != null) {
                            commit('setCurrentUser', {
                                key: key,
                                name: user.displayName,
                                email: user.email,
                                uid: user.uid,
                                status: 1
                            })
                            commit('setAuthName', user.displayName);
                            commit('setAuthEmail', user.email);
                        }
                    })
                }
            })
        },
        // Removing User Data
        removeUserData({ commit }) {
            commit('setCurrentUser', {
                key: "",
                name: "",
                email: "",
                uid: "",
                status: 0
            })
            commit('setAuthName', "");
            commit('setAuthEmail', "");
            commit('forum/storeForums', [],{root:true});
        }

    }
}