
import api from "../../api/api"


export default{
  state: {
    userPosts: [],
    postDetails: "",
    forumPosts: ""
  },
  getters: {
    
    getRespectivePosts(state) {
      return state.forumPosts;
    }
  },
  mutations: {
   
    setUserPosts(state, payload) {
      state.userPosts = payload;
    },
   
    setForumPosts(state, payload) {
      state.forumPosts = payload
    },
    setPostDetails(state, payload) {
      state.postDetails = payload
    }
  },
  actions: {
   
    forumPosts({ commit }, payload) {
      api.fetchPostsByKey(payload.route.params.id, function (response) {
        if (response) {
          commit('setForumPosts', response)
        }
        else{
            commit('setForumPosts',"")
        }
      });
    },
    postDetail({ commit }, payload) {
      api.retrieveRespectivePostByKey(payload.route.params.id, function (post) {
        if (post) {
          commit('setPostDetails', post)
        }
      });
    },
    updateViews({commit},payload){
      api.updateViews(payload.route.params.id)
    }
  }
}
