import api from "../../api/api"

export default {
  state: {
    userForums: [],
    forums: [],
    forumDetails: "",
   
  },
  getters: {
   
    getUserForums(state) {
      return state.userForums;
    },
    getForumDetails(state) {
      return state.forumDetails;
    },
   
  },
  mutations: {
    setUserForums(state, payload) {
      state.userForums = payload;
    },
    storeForums(state, payload) {
      if (payload != null && payload != "") {
        state.forums.push(payload);
      }
      else {
        state.forums = payload;
      }
    },
    setForumDetails(state, payload) {
      state.forumDetails = payload
    },
  
  },
  actions: {
   
    getUserForums({ commit }, user) {
      api.fetchForumbyUID(user.id, (response) => {
        if (response) {
          commit('setUserForums', response);
        } else {
          commit('setUserForums', []);
        }
      })
    },
    forumDetails({ commit }, payload) {
      api.fetchForumByKey(payload.route.params.id, function (response) {
        if (response) {
          commit('setForumDetails', response)
        }
      });
    },
   
    getForums({ commit }) {
      commit('storeForums', [])
      api.loadAllForums((response) => {
        commit('storeForums', response)
      });
    },
  }
}
