import Vue from "vue";
import Vuex from "vuex";
import api from "./api/api";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    auth: {
      email: "",
      fullname: ""
    },
    currentUser: {
      id: "",
      name: "",
      email: "",
      uid: "",
      status: 0
    },
    firbaseLoggedinUser: "",
    userForums: [],
    forums: [],
    userPosts: [],
    forumDetails: "",
    postDetails: "",
    forumPosts: ""
  },
  getters: {
    getCurrentUser(state) {
      return state.currentUser;
    },
    getUserForums(state) {
      return state.userForums;
    },
    getForumDetails(state) {
      return state.forumDetails;
    },
    getRespectivePosts(state) {
      return state.forumPosts;
    }
  },
  mutations: {
    setAuthEmail(state, data) {
      state.auth.email = data
    },
    setAuthName(state, data) {
      state.auth.fullname = data
    },
    setCurrentUser(state, currUserData) {
      state.currentUser.id = currUserData.key,
        state.currentUser.name = currUserData.name,
        state.currentUser.email = currUserData.email,
        state.currentUser.uid = currUserData.uid,
        state.currentUser.status = currUserData.status
    },
    setfirebaseLoggedinUser(state, user) {
      state.firbaseLoggedinUser = user;
    },
    setUserForums(state, payload) {
      state.userForums = payload;
    },
    storeForums(state, payload) {
      if (payload != null && payload != "") {
        state.forums.push(payload);
      }
      else {
        state.forums = payload;
      }
    },
    setUserPosts(state, payload) {
      state.userPosts = payload;
    },
    setForumDetails(state, payload) {
      state.forumDetails = payload
    },
    setForumPosts(state, payload) {
      state.forumPosts = payload
    },
    setPostDetails(state, payload) {
      state.postDetails = payload
    }
  },
  actions: {
    // on refresh get current user and store it in the state
    getCurrentUser({ commit }) {
      api.getCurrentUser((user) => {
        if (user) {
          api.fetchUserByUID(user.uid, (key, val) => {
            if (key != null && val != null) {
              commit('setCurrentUser', {
                key: key,
                name: user.displayName,
                email: user.email,
                uid: user.uid,
                status: 1
              })
              commit('setAuthName', user.displayName);
              commit('setAuthEmail', user.email);
            }
          })
        }
      })
    },
    removeUserData({ commit }) {
      commit('setCurrentUser', {
        key: "",
        name: "",
        email: "",
        uid: "",
        status: 0
      })
      commit('setAuthName', "");
      commit('setAuthEmail', "");
      commit('storeForums', []);
    },
    getUserForums({ commit }, user) {
      api.fetchForumbyUID(user.id, (response) => {
        if (response) {
          console.log("fetching forums by user KEY", response);
          commit('setUserForums', response);
        } else {
          commit('setUserForums', []);
        }
      })
    },
    forumDetails({ commit }, payload) {
      api.fetchForumByKey(payload.route.params.id, function (response) {
        if (response) {
          commit('setForumDetails', response)
        }
      });
    },
    forumPosts({ commit }, payload) {
      api.fetchPostsByKey(payload.route.params.id, function (response) {
        if (response) {
          commit('setForumPosts', response)
        }
      });
    },
    postDetail({ commit }, payload) {
      api.retrieveRespectivePostByKey(payload.route.params.id, function (post) {
        if (post) {
          commit('setPostDetails', post)
        }
      });
    },
    getForums({ commit }) {
      commit('storeForums', [])
      api.loadAllForums((response) => {
        commit('storeForums', response)
      });
    },
  }
});
